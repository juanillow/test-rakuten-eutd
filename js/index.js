
  // slider
  var $item = $('.carousel-item');
  var $wHeight = $(window).height();
  $item.eq(0).addClass('active');
  $item.height($wHeight);
  $item.addClass('full-screen');

  $('.carousel img').each(function () {
    var $src = $(this).attr('src');
    var $color = $(this).attr('data-color');
    $(this).parent().css({
      'background-image': 'url(' + $src + ')',
      'background-color': $color
    });
    $(this).remove();
  });

  $(window).on('resize', function () {
    $wHeight = $(window).height();
    $item.height($wHeight);
  });

  $('.carousel').carousel({
    interval: 6000,
    pause: "false"
  });


//player



  window.onload = function musica() {
    var playButton = document.querySelector('#play');
    var pauseButton = document.querySelector('#pause');
    var audio = new Audio('assets/doriancualquierotraparte.mp3');
    //audio.play();

    
    playButton.addEventListener("click", playAudio, false);
  
    function playAudio() {
      this.style.display = "none";
      pauseButton.style.display = "block";
      audio.play();
    }
  
    pauseButton.addEventListener("click", pauseAudio, false);
  
    function pauseAudio() {
      this.style.display = "none";
      playButton.style.display = "block";
      audio.pause();
    }
  
    audio.addEventListener("timeupdate", controlTime, false);
  
    function controlTime() {
      var seeking = this.currentTime;
      var minutes = Math.floor(this.currentTime / 60);
      var seconds = Math.floor(this.currentTime - minutes * 60);
      var calcs = this.duration;
      var total = this.currentTime / calcs * 100;
      var progressBarUpdate = document.querySelector('.progress-bar');
      progressBarUpdate.style.width = total + "%";
      progressBarUpdate.style.background = "#ffffff";
  
      if (seconds < 10) {
        seconds = "0" + seconds;
      }
  
      var streamTime = document.querySelector('.current-time');
      streamTime.innerText = minutes + ":" + seconds;
  
    }
  
    audio.addEventListener("loadeddata", defaultValues, false);
  
    function defaultValues() {
      var minutes = Math.floor(this.duration / 60);
      var seconds = Math.floor(this.duration - minutes * 60);
      if (seconds < 10) {
        seconds = "0" + seconds;
      }
      var trackTime = document.querySelector('.track-time');
      trackTime.innerText = minutes + ":" + seconds;
    }
  
    audio.onended = function() {
      //Reset Play Button
      pauseButton.style.display = "none";
      playButton.style.display = "block";
      //Reset Progress Bar
      var progressBarUpdate = document.querySelector('.progress-bar');
      progressBarUpdate.style.width = "0%";
      //Reset Time
      var streamTime = document.querySelector('.current-time');
      streamTime.innerText = "0" + ":" + "00";
    };
  
    volumeControls(0.4, 2);
  
    function volumeControls(volumer, index) {
      var childrens = document.querySelectorAll(".volume div");
      var filled = document.querySelectorAll(".fill");
      if (filled.length > index - 1) {
        for (var j = index; j < childrens.length; j++) {
          if (childrens[j].className == "fill") {
            childrens[j].className = "zero-fill";
          }
        }
      }
  
      //Volume controls
      audio.volume = volumer;
  
      var defaultVolume = audio.volume;
      var solidVolume = defaultVolume * 100 / 10;
      var upDown = document.querySelectorAll('.volume div');
  
      if (solidVolume % 2 == 0) {
        for (var i = 0; i < solidVolume / 2; i++) {
          upDown[i].className = "fill";
        }
      } else if (solidVolume == 5) {
        for (var i = 0; i < solidVolume; i++) {
          upDown[i].className = "fill";
        }
      } else if (solidVolume == 0) {
        for (var i = 0; i < 5; i++) {
          upDown[i].className = "zero-fill";
        }
      }
  
      for (var j = 0; j < upDown.length; j++) {
        upDown[j].addEventListener("click", riseUpDown, false);
      }
  
    }
  
    function riseUpDown() {
      var currentElement = this;
      var childrens = this.parentElement.children;
      for (var i = 0; i <= childrens.length; i++) {
        if (this == childrens[i]) {
          var index = i + 1;
          var volumer = index * 2 / 10;
        }
      }
      volumeControls(volumer, index);
    }
  
  };



//   // modal
//   $('.add-to-cart').click(function() {
//     var trigger = $(this);
    
//     showLoader(trigger);
    
//     setTimeout(function () {
//         $('#exampleModal').modal('hide');
//         if(trigger.data('trigger') === 'obsolete') {
//            $('#secondaryModal').modal('show');
//         }
//         if(trigger.data('trigger') != 'obsolete') {
//             alert('Product added to cart!');
//         }
//         hideLoader(trigger);
//     }, Math.floor(Math.random() * 5000));
// });

// $('.add-obsolete').click(function() {
//     var trigger = $(this);
    
//     showLoader(trigger);
    
//     setTimeout(function () {
//         $('#secondaryModal').modal('hide');
        
//         alert('Obsolete product added to cart!');
        
//         hideLoader(trigger);
//     }, Math.floor(Math.random() * 5000));
// });

// function showLoader(button) {
//     button.removeClass('btn-primary').addClass('btn-success');
    
//     button.find('span').addClass('d-none');
    
//     button.find('.lds-ring').removeClass('d-none');
// };

// function hideLoader(button) {
//     button.removeClass('btn-success').addClass('btn-primary');
    
//     button.find('span').removeClass('d-none');
    
//     button.find('.lds-ring').addClass('d-none');
// };



// function uno()
// {
//
// }

// function dos()
// {
// .......
// }

// function funciones()
// {
// uno();
// dos();
// }